package com.tc.itfarm.web.vo;

import com.tc.itfarm.model.Menu;

import java.util.List;

/**
 * Created by wangdongdong on 2016/9/7.
 */
public class MenuVO {

    private Menu menu;

    private Integer count;

    private List<ArticleVO> articles;

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public List<ArticleVO> getArticles() {
        return articles;
    }

    public void setArticles(List<ArticleVO> articles) {
        this.articles = articles;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
