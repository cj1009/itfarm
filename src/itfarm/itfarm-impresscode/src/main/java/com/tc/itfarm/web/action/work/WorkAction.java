package com.tc.itfarm.web.action.work;

import com.tc.itfarm.web.action.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/work")
public class WorkAction extends BaseAction{

	private static final Logger LOGGER = LoggerFactory.getLogger(WorkAction.class);

	/**
	 * 文章首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/index")
	public ModelAndView index(HttpServletRequest request, @RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo) {
		ModelAndView mv = new ModelAndView("work/index");
		this.addAttribute(mv);
		mv.addObject("menuSelected", 2);
		return mv;
	}

}
