package com.tc.itfarm.web.action.article;

import com.tc.itfarm.api.common.JsonMessage;
import com.tc.itfarm.api.util.DateUtils;
import com.tc.itfarm.model.Comment;
import com.tc.itfarm.service.CommentService;
import com.tc.itfarm.web.biz.LoginBiz;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by wangdongdong on 2016/9/20.
 */
@Controller
@RequestMapping("/comment")
public class CommentAction {

    @Resource
    private CommentService commentService;
    @Resource
    private LoginBiz loginBiz;

    @RequestMapping("/save")
    @ResponseBody
    public JsonMessage save(Comment comment) {
        comment.setCreateTime(DateUtils.now());
        comment.setLikeCount(0);
        if (StringUtils.isBlank(comment.getNickName())) {
            comment.setUserId(loginBiz.getCurUser().getRecordId());
            comment.setNickName(loginBiz.getCurUser().getNickname());
        }
        Integer result = commentService.insert(comment);
        return JsonMessage.toResult(result, "操作成功", "操作失败");
    }

}
