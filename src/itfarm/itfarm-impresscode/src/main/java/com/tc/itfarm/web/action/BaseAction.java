package com.tc.itfarm.web.action;

import com.tc.itfarm.api.enums.QueryArticleEnum;
import com.tc.itfarm.service.ArticleService;
import com.tc.itfarm.service.CommentService;
import com.tc.itfarm.web.biz.ArticleBiz;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * Created by wangdongdong on 2016/9/30.
 */
public abstract class BaseAction {

    @Resource
    protected ArticleService articleService;
    @Resource
    protected ArticleBiz articleBiz;
    @Resource
    protected CommentService commentService;

    /**
     * 初始化数据
     * @param mv
     */
    protected void addAttribute(ModelAndView mv) {
        // 用于图片滑动展示
        mv.addObject("animations", articleBiz.getArticleVOs(articleService.selectArticles(QueryArticleEnum.PAGE_VIEW, 5)));
        mv.addObject("newArticles", articleBiz.getArticleVOs(articleService.selectArticles(QueryArticleEnum.CREATE_TIME, 5)));
        // 最新评论
        mv.addObject("comments", commentService.selectRecent10());
    }

}
