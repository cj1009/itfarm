<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title> ${config.webTitle} </title>
    <meta charset="utf-8">
</head>
<body>


<c:forEach items="${articles }" var="item" varStatus="vs">
    <div class="row panel-info" style="height: 250px;">
        <div class="col-md-4 panel" style="height: 100%; text-align: center">
            <div style="width: 220px; height: 180px; margin-top: 30px; text-align: center; overflow: hidden">
                <img src="${ctx}/titleImg/${item.article.titleImg}" onerror="this.src='${ctx }/images/hello_world.jpg'" alt="" width="220" height="160" style="border: 1px solid #ddd;padding: 5px;background: #fff;">
            </div>
        </div>
        <div class="col-md-8 panel" style="height: 250px;">
            <h2><a href="${ctx }/article/456${item.article.recordId}.html">${item.article.title}</a></h2>

            <p class="meta">
                <span class="glyphicon glyphicon-user"></span>&nbsp;<a
                    style="font-size: 10px;">${item.authorName}</a>&nbsp;&nbsp;
                <span class="glyphicon glyphicon-time"></span>&nbsp;<a
                    style="font-size: 10px;">${item.lastDate}</a>&nbsp;&nbsp;
                <span class="glyphicon glyphicon-comment"></span>&nbsp;<a
                    style="font-size: 10px;">${item.commentCount}</a>&nbsp;&nbsp;
                <span class="glyphicon glyphicon-eye-open"></span>&nbsp;<a
                    style="font-size: 10px;">${item.article.pageView}</a>&nbsp;&nbsp;
                <span class="glyphicon glyphicon-folder-open"></span>&nbsp;<a
                    style="font-size: 10px;">${item.categoryName}</a>&nbsp;&nbsp;
                <span class="glyphicon glyphicon-tag"></span>&nbsp;<a
                    style="font-size: 10px;">${item.article.keyword}</a>&nbsp;&nbsp;
            </p>

            <hr/>
            <p><itfarm:StringCut length="200" strValue="${item.article.content }"></itfarm:StringCut></p>
        </div>
    </div>
</c:forEach>
</body>
</html>