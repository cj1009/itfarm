package com.tc.itfarm.api.util;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageCriteria;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.api.model.SingleTableDao;

/**
 * 
 * @Description: 分页查询工具
 * @author: wangdongdong  
 * @date:   2016年7月5日 上午9:12:16   
 *
 */
public class PageQueryHelper {
	
	/**
	 * 
	 * @author: wangdongdong
	 * @date: 2016年7月5日       
	 * @param page 分页对象
	 * @param criteria 条件
	 * @param pageDao dao 
	 * @return
	 */
	public static <U, V extends PageCriteria> PageList<U> queryPage(Page page, V criteria, SingleTableDao<U,V> pageDao, String orderByClause) {
		PageList<U> pageList = new PageList<>();
		criteria.setPage(page);
		if (StringUtils.isNotBlank(orderByClause)) {
			criteria.setOrderByClause(orderByClause);
		}
		List<U> data = pageDao.selectByCriteria(criteria);
		int count = pageDao.countByCriteria(criteria);
		page.setTotalRecords(count);
		pageList.setData(data);
		pageList.setPage(page);
		
		return pageList;
	}
}
