<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
<div id="dcWrap">
 <jsp:include page="../header.jsp"></jsp:include>
 <div id="dcMain">
   <!-- 当前位置 -->
<div id="urHere">ITFARM 管理中心<b>></b><strong>添加分类</strong> </div>   <div class="mainBox" style="height:auto!important;height:550px;min-height:550px;">
            <h3><a href="${ctx }/category/list.do" class="actionBtn">文章分类</a>添加分类</h3>

  <form action="${ctx }/category/addCategory.do" method="post">
     <table width="100%" border="0" cellpadding="8" cellspacing="0" class="tableBasic">
      <tr>
       <td width="80" align="right">分类名称</td>
       <td>
        <input type="text" name="name" value="${name}${category.name}" size="40" class="inpMain" />
           <strong style="color: red;">${msg}</strong>
       </td>
      </tr>
      <tr>
       <td align="right">简单描述</td>
       <td>
        <textarea name="description" cols="60" rows="4" class="textArea">${description}${category.description}</textarea>
       </td>
      </tr>
      <tr>
       <td></td>
       <td>
        <input type="hidden" name="recordId" value="${category.recordId}" />
        <input name="submit" class="btn" type="submit" value="提交" />
       </td>
      </tr>
     </table>
    </form>
       </div>
 </div>
 <div class="clear"></div>
<jsp:include page="../footer.jsp"></jsp:include>
<div class="clear"></div> </div>
</body>
</html>