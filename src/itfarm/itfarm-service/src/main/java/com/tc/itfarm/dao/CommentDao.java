package com.tc.itfarm.dao;

import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.model.Comment;
import com.tc.itfarm.model.CommentCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CommentDao extends SingleTableDao<Comment, CommentCriteria> {
}